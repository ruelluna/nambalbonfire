<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/*
	Copyright (c) 2011-2012 Lonnie Ezell

	Permission is hereby granted, free of charge, to any person obtaining a copy
	of this software and associated documentation files (the "Software"), to deal
	in the Software without restriction, including without limitation the rights
	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
	copies of the Software, and to permit persons to whom the Software is
	furnished to do so, subject to the following conditions:

	The above copyright notice and this permission notice shall be included in
	all copies or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
	THE SOFTWARE.
*/

//------------------------------------------------------------------------
// User Meta Fields Config - These are just examples of various options
// The following examples show how to use regular inputs, select boxes,
// state and country select boxes.
//------------------------------------------------------------------------


// $lang['user_meta_member_code']			= 'Member Code';
// $lang['user_meta_upline_member_code']	= 'Upline Member Code';
// $lang['user_meta_cellular']				= 'Cellular';
// $lang['user_meta_phone']				= 'Phone';

$config['user_meta_fields'] =  array(
	// array(
	// 	'name'   => 'member_code',
	// 	'label'   => lang('user_meta_member_code'),
	// 	'rules'   => 'trim|max_length[100]|xss_clean',
	// 	// 'frontend' => TRUE,
	// 	'admin_only' => TRUE,
	// 	'form_detail' => array(
	// 		'type' => 'input',
	// 		'settings' => array(
	// 			'name'		=> 'member_code',
	// 			'id'		=> 'member_code',
	// 			'maxlength'	=> '100',
	// 			'class'		=> 'span3',
	// 			'required'	=> TRUE,
	// 		),
	// 	),
	// ),


	// array(
	// 	'name'   => 'upline_member_code',
	// 	'label'   => lang('user_meta_upline_member_code'),
	// 	'rules'   => 'required|trim|xss_clean',
	// 	'form_detail' => array(
	// 		'type' => 'input',
	// 		'settings' => array(
	// 			'name'		=> 'upline_member_code',
	// 			'id'		=> 'upline_member_code',
	// 			'maxlength'	=> '100',
	// 			'class'		=> 'span3',
	// 			'required'	=> TRUE,
	// 		),
	// 	),
	// ),

	// array(
	// 	'name'   => 'cellular',
	// 	'label'   => lang('user_meta_cellular'),
	// 	'rules'   => 'required|trim|max_length[100]|xss_clean',
	// 	'admin_only' => TRUE,
	// 	'form_detail' => array(
	// 		'type' => 'input',
	// 		'settings' => array(
	// 			'name'		=> 'cellular',
	// 			'id'		=> 'cellular',
	// 			'maxlength'	=> '100',
	// 			'class'		=> 'span3',
	// 			// 'required'	=> TRUE,
	// 		),
	// 	),
	// ),

	// array(
	// 	'name'   => 'phone',
	// 	'label'   => lang('user_meta_phone'),
	// 	'rules'   => 'required|trim|max_length[100]|xss_clean',
	// 	'admin_only' => TRUE,
	// 	'form_detail' => array(
	// 		'type' => 'input',
	// 		'settings' => array(
	// 			'name'		=> 'user_meta_cellular',
	// 			'id'		=> 'user_meta_cellular',
	// 			'maxlength'	=> 'phone',
	// 			'class'		=> 'phone',
	// 			// 'required'	=> TRUE,
	// 		),
	// 	),
	// ),


	// array(
	// 	'name'   => 'date_registered',
	// 	'label'   => lang('user_meta_date_registered'),
	// 	'rules'   => 'trim|max_length[100]|xss_clean',
	// 	// 'frontend' => TRUE,
	// 	// 'admin_only' => TRUE,
	// 	'form_detail' => array(
	// 		'type' => 'input',
	// 		'settings' => array(
	// 			'name'		=> 'user_meta_date_registered',
	// 			'id'		=> 'user_meta_date_registered',
	// 			'maxlength'	=> '100',
	// 			'class'		=> 'span3 date',
	// 			// 'required'	=> TRUE,
	// 		),
	// 	),
	// ),

	// //dropdown
	// array(
	// 	'name'   => 'Phone',
	// 	'label'   => lang('user_meta_phone'),
	// 	'rules'   => 'required|xss_clean',
	// 	'frontend' => FALSE,
	// 	'admin_only' => TRUE,
	// 	'form_detail' => array(
	// 		'type' => 'dropdown',
	// 		'settings' => array(
	// 			'name'		=> 'type',
	// 			'id'		=> 'type',
	// 			'class'		=> 'span6',
	// 		),
	// 		'options' =>  array(
 //                  'small'  => 'Small Shirt',
 //                  'med'    => 'Medium Shirt',
 //                  'large'   => 'Large Shirt',
 //                  'xlarge' => 'Extra Large Shirt',
 //                ),
	// 	),
	// ),
);