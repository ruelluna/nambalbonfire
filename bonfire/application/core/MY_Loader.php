<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

/* load the MX_Loader class */
require APPPATH."third_party/MX/Loader.php";
require APPPATH."third_party/Sparks/Loader.php";

//change the name of MY_Loader
class Bonfire_Loader extends MX_Loader {}


class MY_Loader extends Bonfire_Loader{

    public function __construct()
    {
       parent::__construct();
    }

    public function spark($spark, $autoload = array())
    {
          $loader = new Sparks_loader();
          $loader->spark($spark,$autoload);
    }
}

/** load the CI class for Modular Separation **/ (class_exists('CI', FALSE)) OR require dirname(__FILE__).'/Ci.php';