<?php

 class User extends ActiveRecord\Model{

 	static $table_name = 'spc_users';


 	function getUplineMember($id){
 		$thisUser = User::find($id);
 		$upline_member = User::find_by_code($thisUser->sponsor);
 		if ($upline_member) {
 			return $upline_member->display_name;
 		} else {
 			return false;
 		}
 	}

 }