
<div class="container">

	<div class="hero-unit">
		<h1>Site Title</h1>

		<p>Product Description.</p>
	</div>

<p>This application is solely made for <a href="http://www.google.com" target="_blank">Owner</a>.</p>

<p>If you want to know about this app please contact us.</p>

<?php if (isset($current_user->email)) : ?>

	<div class="alert alert-info" style="text-align: center">
		<?php echo anchor(SITE_AREA, "Go to Dashboard"); ?>
	</div>

<?php else :?>

	<p style="text-align: center">
		<?php echo anchor('/login', '<i class="icon-lock icon-white"></i> '. lang('bf_action_login'), ' class="btn btn-primary btn-large" '); ?>
	</p>

<?php endif;?>



</div>
