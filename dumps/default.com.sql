/*
 Navicat MySQL Data Transfer

 Source Server         : Local
 Source Server Version : 50525
 Source Host           : localhost
 Source Database       : nambalbonfire

 Target Server Version : 50525
 File Encoding         : utf-8

 Date: 11/04/2013 19:56:27 PM
*/

SET NAMES utf8;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
--  Table structure for `nambal_activities`
-- ----------------------------
DROP TABLE IF EXISTS `nambal_activities`;
CREATE TABLE `nambal_activities` (
  `activity_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NOT NULL DEFAULT '0',
  `activity` varchar(255) NOT NULL,
  `module` varchar(255) NOT NULL,
  `created_on` datetime NOT NULL,
  `deleted` tinyint(12) NOT NULL DEFAULT '0',
  PRIMARY KEY (`activity_id`)
) ENGINE=InnoDB AUTO_INCREMENT=135 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `nambal_activities`
-- ----------------------------
BEGIN;
INSERT INTO `nambal_activities` VALUES ('1', '1', 'logged in from: 127.0.0.1', 'users', '2013-07-04 20:17:16', '1'), ('2', '1', 'Created Module: Checkbox : 127.0.0.1', 'modulebuilder', '2013-07-04 20:27:57', '1'), ('3', '1', 'Created record with ID: 1 : 127.0.0.1', 'checkbox', '2013-07-04 20:28:11', '1'), ('4', '1', 'Updated record with ID: 1 : 127.0.0.1', 'checkbox', '2013-07-04 20:28:17', '1'), ('5', '1', 'Updated record with ID: 1 : 127.0.0.1', 'checkbox', '2013-07-04 20:28:25', '1'), ('6', '1', 'Deleted Module: Checkbox : 127.0.0.1', 'builder', '2013-07-04 20:44:24', '1'), ('7', '1', 'Created Module: Catalog : 127.0.0.1', 'modulebuilder', '2013-07-04 20:49:10', '1'), ('8', '1', 'Created record with ID: 1 : 127.0.0.1', 'catalog', '2013-07-04 20:50:08', '1'), ('9', '1', 'Updated record with ID: 1 : 127.0.0.1', 'catalog', '2013-07-04 21:10:57', '1'), ('10', '1', 'Updated record with ID: 1 : 127.0.0.1', 'catalog', '2013-07-04 21:11:09', '1'), ('11', '1', 'Updated record with ID: 1 : 127.0.0.1', 'catalog', '2013-07-04 21:11:12', '1'), ('12', '1', 'Updated record with ID: 1 : 127.0.0.1', 'catalog', '2013-07-04 21:11:13', '1'), ('13', '1', 'Updated record with ID: 1 : 127.0.0.1', 'catalog', '2013-07-04 21:11:22', '1'), ('14', '1', 'Updated record with ID: 1 : 127.0.0.1', 'catalog', '2013-07-04 21:20:51', '1'), ('15', '1', 'Updated record with ID: 1 : 127.0.0.1', 'catalog', '2013-07-04 21:20:59', '1'), ('16', '1', 'Created record with ID: 2 : 127.0.0.1', 'catalog', '2013-07-04 21:29:30', '1'), ('17', '1', 'Updated record with ID: 1 : 127.0.0.1', 'catalog', '2013-07-04 21:30:48', '1'), ('18', '1', 'Updated record with ID: 1 : 127.0.0.1', 'catalog', '2013-07-04 21:32:14', '1'), ('19', '1', 'Updated record with ID: 1 : 127.0.0.1', 'catalog', '2013-07-04 21:32:22', '1'), ('20', '1', 'Updated record with ID: 1 : 127.0.0.1', 'catalog', '2013-07-04 21:32:45', '1'), ('21', '1', 'Created record with ID: 3 : 127.0.0.1', 'catalog', '2013-07-04 22:11:45', '1'), ('22', '1', 'logged in from: 127.0.0.1', 'users', '2013-07-05 11:09:14', '1'), ('23', '1', 'logged in from: 127.0.0.1', 'users', '2013-07-06 13:30:05', '1'), ('24', '1', 'Created Module: Brand : 127.0.0.1', 'modulebuilder', '2013-07-06 13:32:56', '1'), ('25', '1', 'Created record with ID: 1 : 127.0.0.1', 'brand', '2013-07-06 13:33:25', '1'), ('26', '1', 'Updated record with ID: 1 : 127.0.0.1', 'brand', '2013-07-06 13:35:18', '1'), ('27', '1', 'Updated record with ID: 1 : 127.0.0.1', 'brand', '2013-07-06 13:35:24', '1'), ('28', '1', 'Created Module: Category : 127.0.0.1', 'modulebuilder', '2013-07-06 13:36:59', '1'), ('29', '1', 'Created record with ID: 1 : 127.0.0.1', 'category', '2013-07-06 13:37:19', '1'), ('30', '1', 'Created record with ID: 2 : 127.0.0.1', 'category', '2013-07-06 13:38:00', '1'), ('31', '1', 'Created record with ID: 3 : 127.0.0.1', 'category', '2013-07-06 13:38:05', '1'), ('32', '1', 'Created record with ID: 4 : 127.0.0.1', 'category', '2013-07-06 13:38:12', '1'), ('33', '1', 'Created record with ID: 5 : 127.0.0.1', 'category', '2013-07-06 13:38:35', '1'), ('34', '1', 'created a new User: Xy-za Pesigan', 'users', '2013-07-06 14:15:29', '1'), ('35', '1', 'created a new User: Byron Luna', 'users', '2013-07-06 14:16:01', '1'), ('36', '1', 'modified user: Xy-za Pesigan', 'users', '2013-07-06 14:16:10', '1'), ('37', '1', 'modified user: Byron Luna', 'users', '2013-07-06 14:16:20', '1'), ('38', '1', 'created a new User: Paul Ryan Luna', 'users', '2013-07-06 14:17:07', '1'), ('39', '1', 'modified user: Paul Ryan Luna', 'users', '2013-07-06 14:17:19', '1'), ('40', '1', 'created a new User: Laurine Luna', 'users', '2013-07-06 14:17:55', '1'), ('41', '1', 'modified user: Laurine Luna', 'users', '2013-07-06 14:18:03', '1'), ('42', '2', 'logged in from: 127.0.0.1', 'users', '2013-07-06 14:24:45', '1'), ('43', '2', 'logged out from: 127.0.0.1', 'users', '2013-07-06 14:25:19', '1'), ('44', '1', 'modified user: Paul Ryan Luna', 'users', '2013-07-06 14:25:49', '1'), ('45', '5', 'logged in from: 127.0.0.1', 'users', '2013-07-06 14:26:28', '1'), ('46', '5', 'logged out from: 127.0.0.1', 'users', '2013-07-06 14:27:25', '1'), ('47', '1', 'logged in from: 127.0.0.1', 'users', '2013-07-06 14:27:49', '1'), ('48', '1', 'logged out from: 127.0.0.1', 'users', '2013-07-06 14:27:52', '1'), ('49', '5', 'logged in from: 127.0.0.1', 'users', '2013-07-06 14:28:03', '1'), ('50', '5', 'modified user: Laurine Luna', 'users', '2013-07-06 14:28:29', '1'), ('51', '5', 'modified user: Laurine Luna', 'users', '2013-07-06 14:29:50', '1'), ('52', '5', 'modified user: Laurine Luna', 'users', '2013-07-06 14:29:56', '1'), ('53', '5', 'logged out from: 127.0.0.1', 'users', '2013-07-06 14:41:30', '1'), ('54', '5', 'logged in from: 127.0.0.1', 'users', '2013-07-06 14:41:34', '1'), ('55', '5', 'logged out from: 127.0.0.1', 'users', '2013-07-06 14:41:52', '1'), ('56', '5', 'logged in from: 127.0.0.1', 'users', '2013-07-06 14:42:00', '1'), ('57', '5', 'logged out from: 127.0.0.1', 'users', '2013-07-06 14:43:52', '1'), ('58', '5', 'logged in from: 127.0.0.1', 'users', '2013-07-06 14:44:28', '1'), ('59', '1', 'Created Module: Member : 127.0.0.1', 'modulebuilder', '2013-07-06 15:40:22', '1'), ('60', '1', 'App settings saved from: 127.0.0.1', 'core', '2013-07-06 15:52:41', '1'), ('61', '1', 'logged out from: 127.0.0.1', 'users', '2013-07-06 16:42:17', '1'), ('62', '1', 'logged in from: 127.0.0.1', 'users', '2013-07-06 16:42:55', '1'), ('63', '5', 'logged out from: 127.0.0.1', 'users', '2013-07-06 17:01:14', '1'), ('64', '2', 'logged in from: 127.0.0.1', 'users', '2013-07-06 17:01:17', '1'), ('65', '2', 'logged in from: 127.0.0.1', 'users', '2013-07-08 08:16:43', '1'), ('66', '2', 'logged out from: 127.0.0.1', 'users', '2013-07-08 10:13:10', '1'), ('67', '5', 'logged in from: 127.0.0.1', 'users', '2013-07-08 10:27:35', '1'), ('68', '1', 'logged in from: 127.0.0.1', 'users', '2013-07-08 18:33:35', '1'), ('69', '1', 'logged out from: 127.0.0.1', 'users', '2013-07-08 18:47:12', '1'), ('70', '2', 'logged in from: 127.0.0.1', 'users', '2013-07-08 18:47:16', '1'), ('71', '2', 'logged out from: 127.0.0.1', 'users', '2013-07-08 18:50:09', '1'), ('72', '1', 'logged in from: 127.0.0.1', 'users', '2013-07-08 18:50:14', '1'), ('73', '1', 'modified user: Ruel Luna', 'users', '2013-07-08 18:55:12', '1'), ('74', '1', 'logged in from: 127.0.0.1', 'users', '2013-07-08 18:55:31', '1'), ('75', '1', 'modified user: Ruel Luna', 'users', '2013-07-08 18:55:40', '1'), ('76', '1', 'modified user: Ruel Luna', 'users', '2013-07-08 18:58:02', '1'), ('77', '1', 'modified user: Bodik', 'users', '2013-07-08 18:58:15', '1'), ('78', '1', 'modified user: Bodik', 'users', '2013-07-08 19:00:06', '1'), ('79', '1', 'modified user: Ruel Luna', 'users', '2013-07-08 19:00:25', '1'), ('80', '1', 'created a new User: Juan Dela Cruz', 'users', '2013-07-08 19:09:45', '1'), ('81', '5', 'logged out from: 127.0.0.1', 'users', '2013-07-08 19:47:18', '1'), ('82', '2', 'logged in from: 127.0.0.1', 'users', '2013-07-08 19:47:25', '1'), ('83', '1', 'logged in from: 127.0.0.1', 'users', '2013-07-08 20:42:50', '1'), ('84', '2', 'logged in from: 127.0.0.1', 'users', '2013-07-08 23:09:52', '1'), ('85', '1', 'created a new User: mariajuanadizon', 'users', '2013-07-08 23:13:34', '1'), ('86', '1', 'logged out from: 127.0.0.1', 'users', '2013-07-09 00:52:54', '1'), ('87', '1', 'logged in from: 127.0.0.1', 'users', '2013-07-09 08:25:09', '1'), ('88', '1', 'logged in from: 127.0.0.1', 'users', '2013-07-09 18:49:41', '1'), ('89', '1', 'created a new User: juandelacruz', 'users', '2013-07-09 22:42:32', '1'), ('90', '1', 'Add Member: 665 : 127.0.0.1', 'member', '2013-07-10 20:48:23', '1'), ('91', '1', 'Add Member: 666 : 127.0.0.1', 'member', '2013-07-10 20:49:12', '1'), ('92', '5', 'logged in from: 127.0.0.1', 'users', '2013-07-10 20:59:51', '1'), ('93', '5', 'Add Member: 667 : 127.0.0.1', 'member', '2013-07-10 21:00:59', '1'), ('94', '1', 'Add Member: 668 : 127.0.0.1', 'member', '2013-07-10 22:38:48', '1'), ('95', '1', 'Update Member: 668 : 127.0.0.1', 'member', '2013-07-10 23:04:41', '1'), ('96', '5', 'Update Member: 668 : 127.0.0.1', 'member', '2013-07-10 23:05:25', '1'), ('97', '5', 'Update Member: 668 : 127.0.0.1', 'member', '2013-07-10 23:05:35', '1'), ('98', '1', 'Update Member: 668 : 127.0.0.1', 'member', '2013-07-10 23:06:15', '1'), ('99', '1', 'Update Member: 618 : 127.0.0.1', 'member', '2013-07-10 23:14:09', '1'), ('100', '1', 'Update Member: 569 : 127.0.0.1', 'member', '2013-07-10 23:14:35', '1'), ('101', '1', 'Update Member: 668 : 127.0.0.1', 'member', '2013-07-10 23:15:04', '1'), ('102', '1', 'Update Member: 668 : 127.0.0.1', 'member', '2013-07-10 23:15:37', '1'), ('103', '1', 'Update Member: 668 : 127.0.0.1', 'member', '2013-07-10 23:16:04', '1'), ('104', '1', 'Update Member: 668 : 127.0.0.1', 'member', '2013-07-10 23:16:13', '1'), ('105', '1', 'Update Member: 668 : 127.0.0.1', 'member', '2013-07-10 23:16:27', '1'), ('106', '1', 'Update Member: 668 : 127.0.0.1', 'member', '2013-07-10 23:16:38', '1'), ('107', '1', 'Update Member: 668 : 127.0.0.1', 'member', '2013-07-10 23:17:34', '1'), ('108', '1', 'Update Member: 668 : 127.0.0.1', 'member', '2013-07-10 23:17:47', '1'), ('109', '1', 'Update Member: 668 : 127.0.0.1', 'member', '2013-07-10 23:17:57', '1'), ('110', '1', 'Update Member: 668 : 127.0.0.1', 'member', '2013-07-10 23:18:08', '1'), ('111', '1', 'Deleted Member with ID of : 668 : 127.0.0.1', 'catalog', '2013-07-10 23:25:49', '1'), ('112', '1', 'Add Member: 669 : 127.0.0.1', 'member', '2013-07-10 23:26:20', '1'), ('113', '1', 'Deleted Member with ID of : 669 : 127.0.0.1', 'catalog', '2013-07-10 23:26:37', '1'), ('114', '5', 'logged out from: 127.0.0.1', 'users', '2013-07-13 22:30:56', '1'), ('115', '1', 'logged in from: 127.0.0.1', 'users', '2013-07-13 22:31:19', '1'), ('116', '1', 'Migrate Type: item_ to Version: 2 from: 127.0.0.1', 'migrations', '2013-07-13 23:01:26', '1'), ('117', '1', 'Created record with ID: 1 : 127.0.0.1', 'item', '2013-07-14 00:26:35', '1'), ('118', '1', 'Migrate Type: item_ Uninstalled Version: 0 from: 127.0.0.1', 'migrations', '2013-07-14 00:40:26', '1'), ('119', '1', 'Migrate Type: item_ to Version: 2 from: 127.0.0.1', 'migrations', '2013-07-14 00:40:32', '1'), ('120', '1', 'Created record with ID: 1 : 127.0.0.1', 'item', '2013-07-14 00:57:08', '1'), ('121', '1', 'logged in from: 127.0.0.1', 'users', '2013-08-15 10:59:38', '1'), ('122', '1', 'App settings saved from: 127.0.0.1', 'core', '2013-08-15 11:05:48', '1'), ('123', '1', 'logged in from: 127.0.0.1', 'users', '2013-10-18 16:23:08', '1'), ('124', '1', 'modified user: Super Admin', 'users', '2013-10-18 16:29:18', '1'), ('125', '1', 'logged in from: 127.0.0.1', 'users', '2013-10-18 16:30:12', '1'), ('126', '1', 'logged out from: 127.0.0.1', 'users', '2013-10-18 16:36:33', '1'), ('127', '1', 'logged in from: ::1', 'users', '2013-11-04 19:47:46', '1'), ('128', '1', 'modified user: Super Admin', 'users', '2013-11-04 19:48:07', '1'), ('129', '1', 'logged in from: ::1', 'users', '2013-11-04 19:48:25', '1'), ('130', '1', 'deleted 100 activities', 'activities', '2013-11-04 19:48:40', '1'), ('131', '1', 'deleted 30 activities', 'activities', '2013-11-04 19:48:45', '1'), ('132', '1', 'deleted 1 activities', 'activities', '2013-11-04 19:48:50', '0'), ('133', '1', 'Log settings modified from: ::1', 'logs', '2013-11-04 19:54:44', '0'), ('134', '1', 'Log settings modified from: ::1', 'logs', '2013-11-04 19:54:55', '0');
COMMIT;

-- ----------------------------
--  Table structure for `nambal_email_queue`
-- ----------------------------
DROP TABLE IF EXISTS `nambal_email_queue`;
CREATE TABLE `nambal_email_queue` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `to_email` varchar(128) NOT NULL,
  `subject` varchar(255) NOT NULL,
  `message` text NOT NULL,
  `alt_message` text,
  `max_attempts` int(11) NOT NULL DEFAULT '3',
  `attempts` int(11) NOT NULL DEFAULT '0',
  `success` tinyint(1) NOT NULL DEFAULT '0',
  `date_published` datetime DEFAULT NULL,
  `last_attempt` datetime DEFAULT NULL,
  `date_sent` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `nambal_login_attempts`
-- ----------------------------
DROP TABLE IF EXISTS `nambal_login_attempts`;
CREATE TABLE `nambal_login_attempts` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(40) NOT NULL,
  `login` varchar(50) NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `nambal_permissions`
-- ----------------------------
DROP TABLE IF EXISTS `nambal_permissions`;
CREATE TABLE `nambal_permissions` (
  `permission_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `description` varchar(100) NOT NULL,
  `status` enum('active','inactive','deleted') DEFAULT 'active',
  PRIMARY KEY (`permission_id`)
) ENGINE=InnoDB AUTO_INCREMENT=84 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `nambal_permissions`
-- ----------------------------
BEGIN;
INSERT INTO `nambal_permissions` VALUES ('1', 'Site.Signin.Allow', 'Allow users to login to the site', 'active'), ('2', 'Site.Content.View', 'Allow users to view the Content Context', 'active'), ('3', 'Site.Reports.View', 'Allow users to view the Reports Context', 'active'), ('4', 'Site.Settings.View', 'Allow users to view the Settings Context', 'active'), ('5', 'Site.Developer.View', 'Allow users to view the Developer Context', 'active'), ('6', 'Bonfire.Roles.Manage', 'Allow users to manage the user Roles', 'active'), ('7', 'Bonfire.Users.Manage', 'Allow users to manage the site Users', 'active'), ('8', 'Bonfire.Users.View', 'Allow users access to the User Settings', 'active'), ('9', 'Bonfire.Users.Add', 'Allow users to add new Users', 'active'), ('10', 'Bonfire.Database.Manage', 'Allow users to manage the Database settings', 'active'), ('11', 'Bonfire.Emailer.Manage', 'Allow users to manage the Emailer settings', 'active'), ('12', 'Bonfire.Logs.View', 'Allow users access to the Log details', 'active'), ('13', 'Bonfire.Logs.Manage', 'Allow users to manage the Log files', 'active'), ('14', 'Bonfire.Emailer.View', 'Allow users access to the Emailer settings', 'active'), ('15', 'Site.Signin.Offline', 'Allow users to login to the site when the site is offline', 'active'), ('16', 'Bonfire.Permissions.View', 'Allow access to view the Permissions menu unders Settings Context', 'active'), ('17', 'Bonfire.Permissions.Manage', 'Allow access to manage the Permissions in the system', 'active'), ('18', 'Bonfire.Roles.Delete', 'Allow users to delete user Roles', 'active'), ('19', 'Bonfire.Modules.Add', 'Allow creation of modules with the builder.', 'active'), ('20', 'Bonfire.Modules.Delete', 'Allow deletion of modules.', 'active'), ('21', 'Permissions.Administrator.Manage', 'To manage the access control permissions for the Administrator role.', 'active'), ('22', 'Permissions.Editor.Manage', 'To manage the access control permissions for the Editor role.', 'active'), ('24', 'Permissions.User.Manage', 'To manage the access control permissions for the User role.', 'active'), ('25', 'Permissions.Developer.Manage', 'To manage the access control permissions for the Developer role.', 'active'), ('27', 'Activities.Own.View', 'To view the users own activity logs', 'active'), ('28', 'Activities.Own.Delete', 'To delete the users own activity logs', 'active'), ('29', 'Activities.User.View', 'To view the user activity logs', 'active'), ('30', 'Activities.User.Delete', 'To delete the user activity logs, except own', 'active'), ('31', 'Activities.Module.View', 'To view the module activity logs', 'active'), ('32', 'Activities.Module.Delete', 'To delete the module activity logs', 'active'), ('33', 'Activities.Date.View', 'To view the users own activity logs', 'active'), ('34', 'Activities.Date.Delete', 'To delete the dated activity logs', 'active'), ('35', 'Bonfire.UI.Manage', 'Manage the Bonfire UI settings', 'active'), ('36', 'Bonfire.Settings.View', 'To view the site settings page.', 'active'), ('37', 'Bonfire.Settings.Manage', 'To manage the site settings.', 'active'), ('38', 'Bonfire.Activities.View', 'To view the Activities menu.', 'active'), ('39', 'Bonfire.Database.View', 'To view the Database menu.', 'active'), ('40', 'Bonfire.Migrations.View', 'To view the Migrations menu.', 'active'), ('41', 'Bonfire.Builder.View', 'To view the Modulebuilder menu.', 'active'), ('42', 'Bonfire.Roles.View', 'To view the Roles menu.', 'active'), ('43', 'Bonfire.Sysinfo.View', 'To view the System Information page.', 'active'), ('44', 'Bonfire.Translate.Manage', 'To manage the Language Translation.', 'active'), ('45', 'Bonfire.Translate.View', 'To view the Language Translate menu.', 'active'), ('46', 'Bonfire.UI.View', 'To view the UI/Keyboard Shortcut menu.', 'active'), ('47', 'Bonfire.Update.Manage', 'To manage the Bonfire Update.', 'active'), ('48', 'Bonfire.Update.View', 'To view the Developer Update menu.', 'active'), ('49', 'Bonfire.Profiler.View', 'To view the Console Profiler Bar.', 'active'), ('50', 'Bonfire.Roles.Add', 'To add New Roles', 'active');
COMMIT;

-- ----------------------------
--  Table structure for `nambal_role_permissions`
-- ----------------------------
DROP TABLE IF EXISTS `nambal_role_permissions`;
CREATE TABLE `nambal_role_permissions` (
  `role_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`role_id`,`permission_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `nambal_role_permissions`
-- ----------------------------
BEGIN;
INSERT INTO `nambal_role_permissions` VALUES ('1', '1'), ('1', '2'), ('1', '3'), ('1', '4'), ('1', '5'), ('1', '6'), ('1', '7'), ('1', '8'), ('1', '9'), ('1', '10'), ('1', '11'), ('1', '12'), ('1', '13'), ('1', '14'), ('1', '15'), ('1', '16'), ('1', '17'), ('1', '18'), ('1', '19'), ('1', '20'), ('1', '21'), ('1', '22'), ('1', '24'), ('1', '25'), ('1', '27'), ('1', '28'), ('1', '29'), ('1', '30'), ('1', '31'), ('1', '32'), ('1', '33'), ('1', '34'), ('1', '35'), ('1', '36'), ('1', '37'), ('1', '38'), ('1', '39'), ('1', '40'), ('1', '41'), ('1', '42'), ('1', '43'), ('1', '44'), ('1', '45'), ('1', '46'), ('1', '47'), ('1', '48'), ('1', '49'), ('1', '50'), ('2', '1'), ('2', '2'), ('2', '3'), ('4', '1'), ('6', '1'), ('6', '2'), ('6', '3'), ('6', '4'), ('6', '5'), ('6', '6'), ('6', '7'), ('6', '8'), ('6', '9'), ('6', '10'), ('6', '11'), ('6', '12'), ('6', '13'), ('6', '49');
COMMIT;

-- ----------------------------
--  Table structure for `nambal_roles`
-- ----------------------------
DROP TABLE IF EXISTS `nambal_roles`;
CREATE TABLE `nambal_roles` (
  `role_id` int(11) NOT NULL AUTO_INCREMENT,
  `role_name` varchar(60) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `default` tinyint(1) NOT NULL DEFAULT '0',
  `can_delete` tinyint(1) NOT NULL DEFAULT '1',
  `login_destination` varchar(255) NOT NULL DEFAULT '/',
  `deleted` int(1) NOT NULL DEFAULT '0',
  `default_context` varchar(255) NOT NULL DEFAULT 'content',
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `nambal_roles`
-- ----------------------------
BEGIN;
INSERT INTO `nambal_roles` VALUES ('1', 'Administrator', 'Has full control over every aspect of the site.', '0', '0', '', '0', 'content'), ('2', 'Editor', 'Can handle day-to-day management, but does not have full power.', '0', '1', '', '0', 'content'), ('4', 'User', 'This is the default user with access to login.', '1', '0', '', '0', 'content'), ('6', 'Developer', 'Developers typically are the only ones that can access the developer tools. Otherwise identical to Administrators, at least until the site is handed off.', '0', '1', '', '0', 'content'), ('7', 'Manager', 'Manage the whole system.', '0', '1', '/admin/content', '1', 'content'), ('8', 'Employee', 'Everyday user of the system. The one who will take orders.', '0', '1', '/admin/transact', '1', 'transact');
COMMIT;

-- ----------------------------
--  Table structure for `nambal_schema_version`
-- ----------------------------
DROP TABLE IF EXISTS `nambal_schema_version`;
CREATE TABLE `nambal_schema_version` (
  `type` varchar(40) NOT NULL,
  `version` int(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `nambal_schema_version`
-- ----------------------------
BEGIN;
INSERT INTO `nambal_schema_version` VALUES ('app_', '0'), ('brand_', '2'), ('catalog_', '2'), ('category_', '2'), ('core', '34'), ('item_', '2'), ('member_', '1');
COMMIT;

-- ----------------------------
--  Table structure for `nambal_sessions`
-- ----------------------------
DROP TABLE IF EXISTS `nambal_sessions`;
CREATE TABLE `nambal_sessions` (
  `session_id` varchar(40) NOT NULL DEFAULT '0',
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `user_agent` varchar(120) NOT NULL,
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  `user_data` text,
  PRIMARY KEY (`session_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `nambal_settings`
-- ----------------------------
DROP TABLE IF EXISTS `nambal_settings`;
CREATE TABLE `nambal_settings` (
  `name` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `module` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `value` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`name`),
  UNIQUE KEY `unique - name` (`name`),
  KEY `index - name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
--  Records of `nambal_settings`
-- ----------------------------
BEGIN;
INSERT INTO `nambal_settings` VALUES ('auth.allow_name_change', 'core', '1'), ('auth.allow_register', 'core', '0'), ('auth.allow_remember', 'core', '1'), ('auth.do_login_redirect', 'core', '1'), ('auth.login_type', 'core', 'both'), ('auth.name_change_frequency', 'core', '1'), ('auth.name_change_limit', 'core', '1'), ('auth.password_force_mixed_case', 'core', '0'), ('auth.password_force_numbers', 'core', '0'), ('auth.password_force_symbols', 'core', '0'), ('auth.password_min_length', 'core', '8'), ('auth.password_show_labels', 'core', '0'), ('auth.remember_length', 'core', '1209600'), ('auth.use_extended_profile', 'core', '0'), ('auth.use_usernames', 'core', '0'), ('auth.user_activation_method', 'core', '0'), ('form_save', 'core.ui', 'ctrl+s/⌘+s'), ('goto_content', 'core.ui', 'alt+c'), ('mailpath', 'email', '/usr/sbin/sendmail'), ('mailtype', 'email', 'text'), ('protocol', 'email', 'mail'), ('sender_email', 'email', 'ruelluna@gmail.com'), ('site.languages', 'core', 'a:3:{i:0;s:7:\"english\";i:1;s:7:\"persian\";i:2;s:10:\"portuguese\";}'), ('site.list_limit', 'core', '25'), ('site.show_front_profiler', 'core', '1'), ('site.show_profiler', 'core', '1'), ('site.status', 'core', '1'), ('site.system_email', 'core', 'ruelluna@gmail.com'), ('site.title', 'core', 'Site Title'), ('smtp_host', 'email', ''), ('smtp_pass', 'email', ''), ('smtp_port', 'email', ''), ('smtp_timeout', 'email', ''), ('smtp_user', 'email', ''), ('updates.bleeding_edge', 'core', '1'), ('updates.do_check', 'core', '1');
COMMIT;

-- ----------------------------
--  Table structure for `nambal_user_cookies`
-- ----------------------------
DROP TABLE IF EXISTS `nambal_user_cookies`;
CREATE TABLE `nambal_user_cookies` (
  `user_id` bigint(20) NOT NULL,
  `token` varchar(128) NOT NULL,
  `created_on` datetime NOT NULL,
  KEY `token` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `nambal_user_cookies`
-- ----------------------------
BEGIN;
INSERT INTO `nambal_user_cookies` VALUES ('1', 'y6hQpHoYShnFZofqvxjbFv3N2XNbTPSp7q3A1z71X0XKaDbMVKtk1DjxvNNL5Wz4OOcbwzWPbtLLNHuPU9Vx5htJ5rqYMH2mwyCBHTi2pRoe5qeNlK4U5sLtg8rymMa6', '2013-07-10 18:51:20'), ('1', 'y6hQpHoYShnFZofqvxjbFv3N2XNbTPSp7q3A1z71X0XKaDbMVKtk1DjxvNNL5Wz4OOcbwzWPbtLLNHuPU9Vx5htJ5rqYMH2mwyCBHTi2pRoe5qeNlK4U5sLtg8rymMa6', '2013-07-10 18:51:20');
COMMIT;

-- ----------------------------
--  Table structure for `nambal_user_meta`
-- ----------------------------
DROP TABLE IF EXISTS `nambal_user_meta`;
CREATE TABLE `nambal_user_meta` (
  `meta_id` int(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) NOT NULL DEFAULT '',
  `meta_value` text,
  PRIMARY KEY (`meta_id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `nambal_user_meta`
-- ----------------------------
BEGIN;
INSERT INTO `nambal_user_meta` VALUES ('1', '2', 'state', 'SC'), ('2', '2', 'country', 'US'), ('3', '2', 'type', 'small'), ('4', '3', 'state', 'SC'), ('5', '3', 'country', 'US'), ('6', '3', 'type', 'small'), ('7', '4', 'state', 'SC'), ('8', '4', 'country', 'US'), ('9', '4', 'type', 'small'), ('10', '5', 'state', 'SC'), ('11', '5', 'country', 'US'), ('12', '5', 'type', 'small'), ('13', '1', 'state', 'SC'), ('14', '1', 'country', 'US'), ('15', '1', 'type', 'small');
COMMIT;

-- ----------------------------
--  Table structure for `nambal_users`
-- ----------------------------
DROP TABLE IF EXISTS `nambal_users`;
CREATE TABLE `nambal_users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `role_id` int(11) NOT NULL DEFAULT '4',
  `email` varchar(120) NOT NULL,
  `username` varchar(30) NOT NULL DEFAULT '',
  `password_hash` varchar(40) NOT NULL,
  `reset_hash` varchar(40) DEFAULT NULL,
  `salt` varchar(7) NOT NULL,
  `last_login` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_ip` varchar(40) NOT NULL DEFAULT '',
  `created_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `banned` tinyint(1) NOT NULL DEFAULT '0',
  `ban_message` varchar(255) DEFAULT NULL,
  `reset_by` int(10) DEFAULT NULL,
  `display_name` varchar(255) DEFAULT '',
  `display_name_changed` date DEFAULT NULL,
  `timezone` char(4) NOT NULL DEFAULT 'UM6',
  `language` varchar(20) NOT NULL DEFAULT 'english',
  `active` tinyint(1) NOT NULL DEFAULT '0',
  `activate_hash` varchar(40) NOT NULL DEFAULT '',
  `code` varchar(255) DEFAULT NULL,
  `sponsor` varchar(250) DEFAULT NULL,
  `cellular` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `joining_date` date NOT NULL,
  `position` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `nambal_users`
-- ----------------------------
BEGIN;
INSERT INTO `nambal_users` VALUES ('1', '1', 'developer@nambal.com', 'superadmin', '58ab01491b89968805f9ca560481fc56cc05e4ea', null, 'SoprksN', '2013-11-04 19:48:25', '::1', '0000-00-00 00:00:00', '0', '0', null, null, 'Super Admin', null, 'UM6', 'english', '1', '', 'M30318672', 'M30311118', '9306116822', '', '2012-05-29', 'Franchise');
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
